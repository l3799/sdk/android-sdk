package mt.think.loyalelibrary.repository.network.api_requests

import android.content.Context
import mt.think.loyalelibrary.repository.models.ResponseHolder
import mt.think.loyalelibrary.repository.models.api_response.ApiTransactionResultView
import mt.think.loyalelibrary.repository.network.network_utils.BEARER
import mt.think.loyalelibrary.repository.network.network_utils.RetrofitCreator
import mt.think.loyalelibrary.repository.network.network_utils.performWithErrorHandling

open class PointsBalanceRequests(
    private val baseUrl: String,
    private val applicationContext: Context
) {

    suspend fun getPointBalanceByFiltering(
        token: String,
        schemeId: String,
        filters: String,
        sorts: String,
        page: Int,
        pageSize: Int
    ): ResponseHolder<ArrayList<ApiTransactionResultView>?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getPointBalanceByFiltering(
                    authHeader = BEARER + token,
                    schemeId = schemeId,
                    filters = filters,
                    sorts = sorts,
                    page = page,
                    pageSize = pageSize
                )
        }
    }

}