package mt.think.loyalelibrary.repository.models.api_body

data class ApiLoginRequestModel(val email: String, val password: String)
