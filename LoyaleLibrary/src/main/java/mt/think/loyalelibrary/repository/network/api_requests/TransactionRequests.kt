package mt.think.loyalelibrary.repository.network.api_requests

import android.content.Context
import mt.think.loyalelibrary.repository.models.ResponseHolder
import mt.think.loyalelibrary.repository.models.api_response.ApiGetGainRateResultView
import mt.think.loyalelibrary.repository.models.api_response.ApiTransactionResultView
import mt.think.loyalelibrary.repository.network.network_utils.BEARER
import mt.think.loyalelibrary.repository.network.network_utils.RetrofitCreator
import mt.think.loyalelibrary.repository.network.network_utils.performWithErrorHandling

open class TransactionRequests(
    private val baseUrl: String,
    private val applicationContext: Context
) {

    suspend fun getTransaction(
        id: String,
        schemeId: String,
        token: String
    ): ResponseHolder<ApiTransactionResultView?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getTransaction(id = id, schemeId = schemeId, authHeader = BEARER + token)
        }
    }

    suspend fun getTransactionsWithFilter(
        token: String,
        schemeId: String,
        filters: String,
        sorts: String,
        page: Int,
        pageSize: Int
    ): ResponseHolder<ArrayList<ApiTransactionResultView>?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getTransactionsWithFilter(
                    authHeader = BEARER + token,
                    schemeId = schemeId,
                    filters = filters,
                    sorts = sorts,
                    page = page,
                    pageSize = pageSize
                )
        }
    }

    suspend fun getGainRate(
        outletId: String,
        schemeId: String,
        token: String
    ): ResponseHolder<ApiGetGainRateResultView?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getGainRate(outletId = outletId, schemeId = schemeId, authHeader = BEARER + token)
        }
    }

}