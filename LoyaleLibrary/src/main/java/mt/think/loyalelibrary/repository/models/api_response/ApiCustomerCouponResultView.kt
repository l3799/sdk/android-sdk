package mt.think.loyalelibrary.repository.models.api_response

import mt.think.loyalelibrary.repository.models.other.CouponResultView

data class ApiCustomerCouponResultView(
    val coupon: CouponResultView,
    val id: String,
    val customerId: String?,
    val usesLeft: Int,
    val favorite: Boolean,
    val barCode: String?,
    val rewardId: String,
    val createdDate: String,
    val couponId: String?,
    val from: String,
    val to: String,
    val isUsed: Boolean
)
