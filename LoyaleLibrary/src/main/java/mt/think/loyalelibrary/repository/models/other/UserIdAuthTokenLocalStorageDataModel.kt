package mt.think.loyalelibrary.repository.models.other

data class UserIdAuthTokenLocalStorageDataModel(
    val userId: String,
    val authToken: String
)