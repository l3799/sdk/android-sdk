package mt.think.loyalelibrary.repository.models.other

data class CouponResultView(
    val id: String,
    val schemeId: String,
    val name: String?,
    val description: String?,
    val valid: Boolean,
    val initialNumberOfUses: Int,
    val from: String?,
    val to: String?,
    val until: String?,
    val imageUrl: String?,
    val updatedDate: String,
    val createdDate: String,
    val outletId: ArrayList<String?>,
    val externalReference: String?,
    val platform: String?,
    val isDyanmic: Boolean,
    val dynamicDays: Int?
    )
