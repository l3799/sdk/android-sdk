package mt.think.loyalelibrary.repository.models.api_response

import mt.think.loyalelibrary.repository.models.other.Plan

data class ApiSchemeResultView (
    val id: String,
    val name: String?,
    val currency: String?,
    val description: String?,
    val website: String?,
    val color: String?,
    val imageUrl: String?,
    val pointRedemptionPerCurrency: Double,
    val updatedDate: String,
    val createdDate: String,
    val inLoyale: Boolean,
    val hidden: Boolean,
    val franchises: ArrayList<ApiFranchiseResultView?>,
    val rounding: Int,
    val status: Int,
    val plan: Plan,
    val planId: String,
    val setupCompleted: Boolean,
    val requireTwoFactorAuth: Boolean,
)