package mt.think.loyalelibrary.repository.models.api_response

data class ApiTown(
    val id: String,
    val name: String,
    val countryCode: String
)