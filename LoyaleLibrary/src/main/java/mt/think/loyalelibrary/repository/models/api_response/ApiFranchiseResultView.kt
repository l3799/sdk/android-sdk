package mt.think.loyalelibrary.repository.models.api_response

import mt.think.loyalelibrary.repository.models.other.CategoryResultView
import mt.think.loyalelibrary.repository.models.other.FranchiseOutletResultView

data class ApiFranchiseResultView (
    val id: String,
    val name: String?,
    val description: String?,
    val schemeId: String,
    val imageUrl: String?,
    val imageGallery: ArrayList<String?>,
    val categoriesIds: ArrayList<String?>,
    val categories: ArrayList<CategoryResultView?>,
    val outlets: ArrayList<FranchiseOutletResultView?>,
    val updatedDate: String,
    val createdDate: String,
    val hidden: Boolean
        )