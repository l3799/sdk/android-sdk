package mt.think.loyalelibrary.repository.models.other

import mt.think.loyalelibrary.repository.models.api_response.ApiLevelResultView

data class CustomerWithTransactionDetailsResultView(
    val id: String?,
    val email: String?,
    val firstName: String?,
    val lastName: String,
    val dob: String?,
    val gender: Int,
    val areaCode: String?,
    val mobileNumber: String?,
    val addressLine1: String?,
    val addressLine2: String?,
    val town: String?,
    val state: String?,
    val postCode: String?,
    val country: String?,
    val marketingSub: Boolean,
    val barCode: String?,
    val detailedLevels: ArrayList<LegacyCustomerLevelView?>,
    val updatedDate: String,
    val createdDate: String,
    val lastConnectedDate: String?,
    val lastTransactionDate: String?,
    val alerts: ArrayList<String?>,
    val profileImageUrl: String?,
    val onHold: Boolean,
    val deleted: Boolean,
    val emailVerified: Boolean,
    val mobileNumberVerified: Boolean,
    val signUpPlatform: String?,
    val signUpPlatform2: SignUpPlatformView,
    val pointsFromSplit: Double,
    val monetaryFromSplit: Double
)
