package mt.think.loyalelibrary.repository.models

data class ErrorResponse(
    val code: Int,
    val errorId: String,
    val errors: ArrayList<String>
)