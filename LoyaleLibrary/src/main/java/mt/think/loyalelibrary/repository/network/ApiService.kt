package mt.think.loyalelibrary.repository.network

import mt.think.loyalelibrary.repository.models.api_body.*
import mt.think.loyalelibrary.repository.models.api_response.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    /**
     * Customer
     */

    @POST("api/PushToken")
    suspend fun postFirebaseToken(
        @Header("Scheme") schemeId: String,
        @Body customerData: ApiRequestRegisterPushToken
    ): Response<ApiPushTokenIdResultView>

    @DELETE("api/PushToken")
    suspend fun deleteFirebaseToken(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Query("token") token: String
    ): Response<Any>

    @POST("api/Customer")
    suspend fun createCustomer(
        @Header("Authorization") token: String,
        @Header("Scheme") schemeId: String,
        @Body customerData: ApiCustomerInsertView
    ): Response<ApiCustomerResultView>

    @GET("api/Customer/{id}")
    suspend fun getCustomer(
        @Header("Authorization") token: String,
        @Header("Scheme") schemeId: String,
        @Path("id") customerId: String
    ): Response<ApiCustomerResultView>

    @PUT("api/Customer")
    suspend fun updateCustomer(
        @Header("Authorization") token: String,
        @Header("Scheme") schemeId: String,
        @Body customerData: ApiCustomerPutView
    ): Response<Any>

    @GET("api/Customer/GetAdditional")
    suspend fun getCustomerAdditional(
        @Header("Authorization") token: String,
        @Header("Scheme") schemeId: String,
        @Query("customerId") customerId: String
    ): Response<ApiCustomerWithAdditionalFieldsResultView>

    @Multipart
    @POST("api/Upload/ProfilePicture")
    suspend fun uploadProfilePicture(
        @Header("Authorization") token: String,
        @Header("Scheme") schemeId: String,
        @Query("customerId") customerId: String,
        @Part("description") description: RequestBody,
        @Part file: MultipartBody.Part
    ): Response<Any>

    @POST("api/Customer/JoinScheme/{schemeId}")
    suspend fun joinScheme(
        @Header("Authorization") token: String,
        @Path("schemeId") schemeId: String,
        @Query("customerId") customerId: String
    ): Response<Any>

    @POST("api/Customer/LeaveScheme/{schemeId}")
    suspend fun leaveScheme(
        @Header("Authorization") token: String,
        @Path("schemeId") schemeId: String,
        @Query("customerId") customerId: String
    ): Response<Any>

    @PUT("api/Customer/Suspend/{id}")
    suspend fun suspendUserAccount(
        @Header("Scheme") schemeId: String,
        @Path("id") id: String
    ): Response<Any>


    /**
     * Authentication/Account
     */

    @POST("api/token")
    suspend fun authUser(
        @Header("Scheme") schemeId: String,
        @Body loginToPasswordModel: ApiLoginRequestModel
    ): Response<ApiLoginResponseDataModel>

    @POST("api/PushToken")
    suspend fun registerPushToken(
        @Header("Scheme") schemeId: String,
        @Body pushTokenModel: ApiRequestRegisterPushToken
    ): Response<Any>

    @POST("api/Account/ForgotPassword")
    suspend fun forgotPassword(
        @Header("Scheme") schemeId: String,
        @Body email: ApiEmailDataClass
    ): Response<Any>

    @POST("api/Account/ChangePassword")
    suspend fun changePassword(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Body passwordsData: ApiChangePasswordDataClass
    ): Response<Any>


    /**
     * Coupons
     */

    @GET("api/CouponsLinked/{id}")
    suspend fun getCouponLinked(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Path("id") id: String
    ): Response<ApiCustomerCouponResultView>

    @GET("api/CouponsLinked")
    suspend fun getCouponsLinkedWithFilter(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Query("Filters") filters: String,
        @Query("Sorts") sorts: String,
        @Query("Page") page: Int,
        @Query("PageSize") pageSize: Int
    ): Response<ArrayList<ApiCustomerCouponResultView>>


    /**
     * Alerts
     */

    @GET("api/AlertsLinked/{id}")
    suspend fun getAlertLinked(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Path("id") id: String
    ): Response<ApiAlertLinkedResultView>

    @GET("api/AlertsLinked")
    suspend fun getAlertsLinkedWithFilter(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Query("Filters") filters: String,
        @Query("Sorts") sorts: String,
        @Query("Page") page: Int,
        @Query("PageSize") pageSize: Int
    ): Response<ArrayList<ApiAlertLinkedResultView>>


    /**
     * Transactions
     */

    @GET("api/Transaction/{id}")
    suspend fun getTransaction(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Path("id") id: String
    ): Response<ApiTransactionResultView>

    @GET("api/Transaction")
    suspend fun getTransactionsWithFilter(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Query("Filters") filters: String,
        @Query("Sorts") sorts: String,
        @Query("Page") page: Int,
        @Query("PageSize") pageSize: Int
    ): Response<ArrayList<ApiTransactionResultView>>

    @GET("api/Transaction/GainRate")
    suspend fun getGainRate(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Query("outletId") outletId: String
    ): Response<ApiGetGainRateResultView>


    /**
     * Point Balance
     */

    @GET("api/Transaction")
    suspend fun getPointBalanceByFiltering(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Query("Filters") filters: String,
        @Query("Sorts") sorts: String,
        @Query("Page") page: Int,
        @Query("PageSize") pageSize: Int
    ): Response<ArrayList<ApiPointBalanceResultView>>


    /**
     * Franchise
     */

    @GET("api/Franchise/{id}")
    suspend fun getFranchise(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Path("id") id: String
    ): Response<ApiFranchiseResultView>

    @GET("api/Franchise")
    suspend fun getFranchiseWithFilter(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Query("Filters") filters: String,
        @Query("Sorts") sorts: String,
        @Query("Page") page: Int,
        @Query("PageSize") pageSize: Int
    ): Response<ArrayList<ApiFranchiseResultView>>


    /**
     * Outlet
     */

    @GET("api/Outlet/{id}")
    suspend fun getOutlet(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Path("id") id: String
    ): Response<ApiOutletResultView>

    @GET("api/Outlet")
    suspend fun getOutletWithFilter(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Query("Filters") filters: String,
        @Query("Sorts") sorts: String,
        @Query("Page") page: Int,
        @Query("PageSize") pageSize: Int
    ): Response<ArrayList<ApiOutletResultView>>


    /**
     * Level
     */

    @GET("api/Level/{id}")
    suspend fun getLevel(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Path("id") id: String
    ): Response<ApiLevelResultView>

    @GET("api/Level")
    suspend fun getLevelWithFilter(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Query("Filters") filters: String,
        @Query("Sorts") sorts: String,
        @Query("Page") page: Int,
        @Query("PageSize") pageSize: Int
    ): Response<ArrayList<ApiLevelResultView>>


    /**
     * Tier
     */

    @GET("api/Tier/{id}")
    suspend fun getTier(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Path("id") id: String
    ): Response<ApiTierResultView>

    @GET("api/Tier")
    suspend fun getTiersWithFilter(
        @Header("Authorization") authHeader: String,
        @Header("Scheme") schemeId: String,
        @Query("Filters") filters: String,
        @Query("Sorts") sorts: String,
        @Query("Page") page: Int,
        @Query("PageSize") pageSize: Int
    ): Response<ArrayList<ApiTierResultView>>

    /**
     * Scheme
     */

    @GET("api/Scheme/{id}")
    suspend fun getSchemeById(@Path("id") id: String): Response<ApiSchemeResultView>


    /**
     * Helpers
     */

    @GET("api/helpers/mobilecode")
    suspend fun getMobileCodes(): Response<ArrayList<ApiAreaCodesDataModel>>

    @GET("api/Helpers/Countries")
    suspend fun getCountriesList(): Response<ArrayList<ApiCountryModel>>

    @GET("api/Helpers/Towns")
    suspend fun getTownsByLocale(
        @Header("Scheme") schemeId: String,
        @Query("countryCode") locale: String
    ): Response<ArrayList<ApiTown>>

    @GET("api/Helpers/Currency")
    suspend fun getCurrencyByFilter(): Response<ArrayList<ApiCurrencies>>


    /**
     * Other
     */

    @POST("/api/Message")
    suspend fun sendMessage(
        @Header("Scheme") schemeId: String,
        @Header("Authorization") authHeader: String,
        @Body message: ApiRequestMessage
    ): Response<Any>

    @POST("/api/Contact/match")
    suspend fun matchContactsWithSchemeMembers(
        @Header("Scheme") schemeId: String,
        @Header("Authorization") authHeader: String,
        @Body contacts: ApiRequestMatchContactsDataModel
    ): Response<ApiResponseMatchContactsDataModel>
}