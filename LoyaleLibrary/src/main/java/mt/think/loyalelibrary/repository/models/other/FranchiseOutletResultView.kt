package mt.think.loyalelibrary.repository.models.other

data class FranchiseOutletResultView (
    val id: String,
    val name: String?,
    val coordinate: CoordinateView,
    val address: AddressView,
    val phoneNumber: String?,
    val openingHours: ArrayList<String?>,
    val imageUrl: String?,
    val updatedDate: String,
    val createdDate: String,
    val facebook: String?,
    val instagram: String?,
    val hidden: Boolean
        )