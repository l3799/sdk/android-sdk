package mt.think.loyalelibrary.repository

import android.app.Activity
import android.util.Log
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mt.think.loyalelibrary.repository.models.ResponseHolder
import mt.think.loyalelibrary.repository.models.api_body.*
import mt.think.loyalelibrary.repository.models.api_response.*
import mt.think.loyalelibrary.repository.models.other.PhonebookContactModel
import mt.think.loyalelibrary.repository.network.api_requests.*
import mt.think.loyalelibrary.repository.network.network_utils.BEARER
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

interface LoyaleManager/*(
    val activity: Activity,
    val baseUrl: String,
    val schemeId: String,
    //val dataCash: T
)*/ {

    val activity: Activity
    val baseUrl: String
    val schemeId: String

//    /**
//     * getting locally saved user token
//     * or null if there is no such data
//     * difference between this method and [getUserToken] is
//     * that this one don't try to login with previously saved login data,
//     * just check is there saved token
//     */
//    fun getLocallySavedUserToken(): String? {
//        val userData = LocalStorage(activity).getSavedTokenUserId()
//        return userData?.authToken ?: return null
//    }

//    /**
//     * Trying to get user token.
//     * Checking is there saved in cash one and is it still valid.
//     * If there is no token (or invalid one) trying to get new token
//     * with user’s login/password saved before.
//     * Can return null if any of this will not work out
//     *
//     * @return token string or null in case of there is no saved token or token invalid
//     */
//    suspend fun getUserToken(): String? {
//        val token = getLocallySavedUserToken()
//
//        return token
//
////        return if (token != null && isTokenValid(token)) {
////            token
////        } else {
////            tryToLoginWithSavedUserData()
////        }
//    }

//    /**
//     * checking is there valid token saved before
//     * and perform call of specified method
//     * or return unauth response instance
//     */
//    suspend fun <T> performWithTokenCheck(methodToCall: suspend (token: String) -> ResponseHolder<T>): ResponseHolder<T> {
//        val token = getUserToken()
//
//        return if (token == null) {
//            ResponseHolder.createUnautorizeError()
//        } else {
//            methodToCall(token)
//        }
//    }

    class Customer(
        override val activity: Activity,
        override val baseUrl: String,
        override val schemeId: String
    ) : LoyaleManager {

        suspend fun postFirebaseToken(
            customerData: ApiRequestRegisterPushToken
        ): ResponseHolder<ApiPushTokenIdResultView?> {
            return CustomerRequests(baseUrl, activity).postFirebaseToken(
                schemeId = schemeId,
                customerData = customerData
            )
        }

        suspend fun deleteFirebaseToken(
            authToken: String,
            notificationToken: String
        ): ResponseHolder<Any?> {
            return CustomerRequests(baseUrl, activity).deleteFirebaseToken(
                authToken = authToken,
                schemeId = schemeId,
                notificationToken = notificationToken
            )
        }

        suspend fun createCustomer(
            token: String,
            customerData: ApiCustomerInsertView
        ): ResponseHolder<ApiCustomerResultView?> {
            return CustomerRequests(baseUrl, activity).createCustomer(
                token, schemeId, customerData
            )
        }

        suspend fun getCustomer(
            token: String,
            customerId: String
        ): ResponseHolder<ApiCustomerResultView?> {
            return CustomerRequests(baseUrl, activity).getCustomer(
                token, schemeId, customerId
            )
        }

        suspend fun updateCustomer(
            token: String,
            customerData: ApiCustomerPutView
        ): ResponseHolder<Any?> {
            return CustomerRequests(baseUrl, activity).updateCustomer(
                token, schemeId, customerData
            )
        }

        suspend fun getCustomerAdditional(
            token: String,
            customerId: String
        ): ResponseHolder<ApiCustomerWithAdditionalFieldsResultView?> {
            return CustomerRequests(baseUrl, activity).getCustomerAdditional(
                token, schemeId, customerId
            )
        }

        suspend fun uploadProfileImage(token: String, customerId: String, file: File): ResponseHolder<Any?> {
            val requestFile = file
                .asRequestBody("image/png".toMediaTypeOrNull())
            val body = MultipartBody.Part.createFormData("file", "somename", requestFile)

            val description = "image/png".toRequestBody("multipart/form-data".toMediaTypeOrNull())

            return CustomerRequests(baseUrl, activity).uploadProfileImage(
                token,
                schemeId,
                customerId,
                description,
                body,
            )
        }

        suspend fun joinScheme(
            token: String,
            customerId: String
        ): ResponseHolder<Any?> {
            return CustomerRequests(baseUrl, activity).joinScheme(
                token = token,
                schemeId = schemeId,
                customerId = customerId
            )
        }

        suspend fun leaveScheme(
            token: String,
            customerId: String
        ): ResponseHolder<Any?> {
            return CustomerRequests(baseUrl, activity).leaveScheme(
                token = token,
                schemeId = schemeId,
                customerId = customerId
            )
        }

        suspend fun suspendUserAccount(
            id: String
        ): ResponseHolder<Any?> {
            return CustomerRequests(baseUrl, activity).suspendUserAccount(
                schemeId = schemeId,
                id = id
            )
        }

    }

    class AuthenticationAccount(
        override val activity: Activity,
        override val baseUrl: String,
        override val schemeId: String
    ) : LoyaleManager {

        /**
         * performing login
         */
        suspend fun performLogin(
            email: String,
            password: String
        ): ResponseHolder<ApiLoginResponseDataModel?> {
            val responseHolder = AuthenticationAccountRequests(baseUrl, activity).login(schemeId, email, password)
            if (responseHolder.isSucceed()) {
                registerPushToken(responseHolder)
//                saveUserIdAndAuthToken(responseHolder.payload!!.userId, responseHolder.payload.token)
//                saveUserLoginData(SavedLoginDataModel(email, password))
            }
            return responseHolder
        }

        suspend fun forgotPassword(
            email: ApiEmailDataClass
        ): ResponseHolder<Any?> {
            return AuthenticationAccountRequests(baseUrl, activity).forgotPassword(
                schemeId = schemeId,
                email = email
            )
        }

        suspend fun changePassword(
            token: String,
            passwordsData: ApiChangePasswordDataClass
        ): ResponseHolder<Any?> {
            return AuthenticationAccountRequests(baseUrl, activity).changePassword(
                token = token,
                schemeId = schemeId,
                passwordsData = passwordsData
            )
        }

        /**
         * registering firebase push token to server
         */
        protected suspend fun registerPushToken(responseHolder: ResponseHolder<ApiLoginResponseDataModel?>) {
            try {
                FirebaseInstanceId.getInstance().instanceId
                    .addOnCompleteListener(OnCompleteListener { task ->
                        if (!task.isSuccessful) {
                            return@OnCompleteListener
                        }

                        // Get new Instance ID token
                        val token = task.result?.token
                        //saveNotificationToken(token ?: "")

                        CoroutineScope(Dispatchers.IO).launch {
                            AuthenticationAccountRequests(baseUrl, activity).registerPushToken(
                                schemeId,
                                token ?: "",
                                responseHolder.payload!!.userId
                            )
                        }
                    })
            } catch (e: Exception) {
                Log.e("error: ", e.message!!)
            }
        }
    }

    class Transaction(
        override val activity: Activity,
        override val baseUrl: String,
        override val schemeId: String
    ) : LoyaleManager {
        suspend fun getTransaction(
            id: String,
            token: String
        ): ResponseHolder<ApiTransactionResultView?> {
            return TransactionRequests(baseUrl, activity).getTransaction(
                id = id,
                schemeId = schemeId,
                token = token
            )
        }

        suspend fun getTransactionsWithFilter(
            token: String,
            filters: String,
            sorts: String,
            page: Int,
            pageSize: Int
        ): ResponseHolder<ArrayList<ApiTransactionResultView>?> {
            return TransactionRequests(baseUrl, activity).getTransactionsWithFilter(
                token = token,
                schemeId = schemeId,
                filters = filters,
                sorts = sorts,
                page = page,
                pageSize = pageSize
            )
        }

        suspend fun getGainRate(
            token: String,
            outletId: String,
        ): ResponseHolder<ApiGetGainRateResultView?> {
            return TransactionRequests(baseUrl, activity).getGainRate(
                outletId = outletId,
                schemeId = schemeId,
                token = token
            )
        }
    }

    class PointsBalance(
        override val activity: Activity,
        override val baseUrl: String,
        override val schemeId: String
    ) : LoyaleManager {

        suspend fun getPointBalanceByFiltering(
            token: String,
            filters: String,
            sorts: String,
            page: Int,
            pageSize: Int
        ): ResponseHolder<ArrayList<ApiTransactionResultView>?> {
            return PointsBalanceRequests(baseUrl, activity).getPointBalanceByFiltering(
                token = token,
                schemeId = schemeId,
                filters = filters,
                sorts = sorts,
                page = page,
                pageSize = pageSize
            )
        }

    }

    class Coupon(
        override val activity: Activity,
        override val baseUrl: String,
        override val schemeId: String
    ) : LoyaleManager {
        suspend fun getCouponLinked(
            id: String,
            token: String
        ): ResponseHolder<ApiCustomerCouponResultView?> {
            return CouponRequests(baseUrl, activity).getCouponLinked(
                id = id,
                schemeId = schemeId,
                token = token
            )
        }

        suspend fun getCouponsLinked(
            token: String,
            filters: String,
            sorts: String,
            page: Int,
            pageSize: Int
        ): ResponseHolder<ArrayList<ApiCustomerCouponResultView>?> {
            return CouponRequests(baseUrl, activity).getCouponsLinkedWithFilter(
                token = token,
                schemeId = schemeId,
                filters = filters,
                sorts = sorts,
                page = page,
                pageSize = pageSize
            )
        }
    }

    class Alert(
        override val activity: Activity,
        override val baseUrl: String,
        override val schemeId: String
    ) : LoyaleManager {
        suspend fun getAlertLinked(
            id: String,
            token: String
        ): ResponseHolder<ApiAlertLinkedResultView?> {
            return AlertRequests(baseUrl, activity).getAlertLinked(
                id = id,
                schemeId = schemeId,
                token = token
            )
        }

        suspend fun getAlertsLinkedWithFilter(
            token: String,
            filters: String,
            sorts: String,
            page: Int,
            pageSize: Int
        ): ResponseHolder<ArrayList<ApiAlertLinkedResultView>?> {
            return AlertRequests(baseUrl, activity).getAlertsLinkedWithFilter(
                token = token,
                schemeId = schemeId,
                filters = filters,
                sorts = sorts,
                page = page,
                pageSize = pageSize
            )
        }
    }

    class Franchise(
        override val activity: Activity,
        override val baseUrl: String,
        override val schemeId: String
    ) : LoyaleManager {
        suspend fun getFranchise(
            id: String,
            token: String
        ): ResponseHolder<ApiFranchiseResultView?> {
            return FranchiseRequests(baseUrl, activity).getFranchise(
                id = id, schemeId = schemeId, token = token
            )
        }

        suspend fun getFranchiseWithFilter(
            token: String,
            filters: String,
            sorts: String,
            page: Int,
            pageSize: Int
        ): ResponseHolder<ArrayList<ApiFranchiseResultView>?> {
            return FranchiseRequests(baseUrl, activity).getFranchiseWithFilter(
                token = token,
                schemeId = schemeId,
                filters = filters,
                sorts = sorts,
                page = page,
                pageSize = pageSize
            )
        }
    }

    class Outlet(
        override val activity: Activity,
        override val baseUrl: String,
        override val schemeId: String
    ) : LoyaleManager {
        suspend fun getOutlet(
            id: String,
            token: String
        ): ResponseHolder<ApiOutletResultView?> {
            return OutletRequests(baseUrl, activity).getOutlet(id = id, schemeId = schemeId, token = token)
        }

        suspend fun getOutletWithFilter(
            token: String,
            filters: String,
            sorts: String,
            page: Int,
            pageSize: Int
        ): ResponseHolder<ArrayList<ApiOutletResultView>?> {
            return OutletRequests(baseUrl, activity).getOutletWithFilter(
                token = token,
                schemeId = schemeId,
                filters = filters,
                sorts = sorts,
                page = page,
                pageSize = pageSize
            )
        }
    }

    class Level(
        override val activity: Activity,
        override val baseUrl: String,
        override val schemeId: String
    ) : LoyaleManager {
        suspend fun getLevel(
            id: String,
            token: String
        ): ResponseHolder<ApiLevelResultView?> {
            return LevelRequests(baseUrl, activity).getLevel(id = id, schemeId = schemeId, token = token)
        }

        suspend fun getLevelWithFilter(
            token: String,
            filters: String,
            sorts: String,
            page: Int,
            pageSize: Int
        ): ResponseHolder<ArrayList<ApiLevelResultView>?> {
            return LevelRequests(baseUrl, activity).getLevelWithFilter(
                token = token,
                schemeId = schemeId,
                filters = filters,
                sorts = sorts,
                page = page,
                pageSize = pageSize
            )
        }
    }

    class Group(
        override val activity: Activity,
        override val baseUrl: String,
        override val schemeId: String
    ) : LoyaleManager {
        suspend fun getTier(
            id: String,
            token: String
        ): ResponseHolder<ApiTierResultView?> {
            return GroupRequests(baseUrl, activity).getTier(id = id, schemeId = schemeId, token = token)
        }

        suspend fun getTiersWithFilter(
            token: String,
            filters: String,
            sorts: String,
            page: Int,
            pageSize: Int
        ): ResponseHolder<ArrayList<ApiTierResultView>?> {
            return GroupRequests(baseUrl, activity).getTiersWithFilter(
                token = token,
                schemeId = schemeId,
                filters = filters,
                sorts = sorts,
                page = page,
                pageSize = pageSize
            )
        }
    }

    class Scheme(
        override val activity: Activity,
        override val baseUrl: String,
        override val schemeId: String
    ) : LoyaleManager {
        suspend fun getSchemeById(id: String): ResponseHolder<ApiSchemeResultView?> {
            return SchemeRequests(baseUrl, activity).getSchemeById(
                id
            )
        }
    }

    class Helpers(
        override val activity: Activity,
        override val baseUrl: String,
        override val schemeId: String
    ) : LoyaleManager {
        /**
         * getting area's phone codes list
         */
        suspend fun getAreasCodes(): ResponseHolder<ArrayList<ApiAreaCodesDataModel>?> {
            return HelpersRequests(baseUrl, activity).getMobileCodes()
        }

        suspend fun getCountriesList(): ResponseHolder<ArrayList<ApiCountryModel>?> {
            return HelpersRequests(baseUrl, activity).getCountriesList()
        }

        suspend fun getTownsList(locale: String): ResponseHolder<ArrayList<ApiTown>?> {
            return HelpersRequests(baseUrl, activity).getTownsByLocale(schemeId, locale)
        }

        suspend fun getCurrencyByFilter(): ResponseHolder<ArrayList<ApiCurrencies>?> {
            return HelpersRequests(baseUrl, activity).getCurrencyByFilter()
        }
    }

    class Other(
        override val activity: Activity,
        override val baseUrl: String,
        override val schemeId: String
    ) : LoyaleManager {

        suspend fun sendMessage(message: ApiRequestMessage, token: String): ResponseHolder<Any?> {
            return OtherRequests(baseUrl, activity).sendMessage(
                token,
                schemeId,
                message
            )
            //performWithTokenCheck { token ->
            //}
        }

        /**
         * @return ResponseHolder with HashMap phone number to user (represented as [PhonebookContactModel])
         *         which are part of specified scheme
         */
        suspend fun matchListOfPhoneNumbersWithSchemeUsers(
            contactsList: ArrayList<PhonebookContactModel>,
            token: String
        ): ResponseHolder<HashMap<String, PhonebookContactModel>?> {
            val phoneNumberToPhonebookUser = HashMap<String, PhonebookContactModel>()

            for (contact in contactsList) {
                if (contactsList.isNullOrEmpty()) {
                    continue
                }

                for (phoneNumber in contact.phoneList!!) {
                    if (!phoneNumber.phoneNo.isNullOrEmpty()) {
                        phoneNumberToPhonebookUser[phoneNumber.phoneNo!!] = contact
                    }
                }
            }

            val listOfNumbersResponse = OtherRequests(baseUrl, activity)
                .matchContactsWithSchemeMembers(
                    schemeId,
                    token,
                    ArrayList(phoneNumberToPhonebookUser.keys)
                )
            //performWithTokenCheck { token ->
            //}

            return if (listOfNumbersResponse.isSucceed()) {
                val schemeMembersPhonesList = listOfNumbersResponse.payload!!.contactMatches

                val schemeMembersPhoneToContactMap =
                    phoneNumberToPhonebookUser.filter { (phone, contact) ->
                        schemeMembersPhonesList.contains(phone)
                    }

                ResponseHolder.createSucceedResponse(HashMap(schemeMembersPhoneToContactMap))
            } else {
                ResponseHolder.createFailedResponse(
                    listOfNumbersResponse.errorCode,
                    listOfNumbersResponse.errorMessages
                )
            }
        }
    }
}