package mt.think.loyalelibrary.repository.network.api_requests

import android.content.Context
import mt.think.loyalelibrary.repository.models.ResponseHolder
import mt.think.loyalelibrary.repository.models.api_response.ApiAreaCodesDataModel
import mt.think.loyalelibrary.repository.models.api_response.ApiCountryModel
import mt.think.loyalelibrary.repository.models.api_response.ApiCurrencies
import mt.think.loyalelibrary.repository.models.api_response.ApiTown
import mt.think.loyalelibrary.repository.network.network_utils.RetrofitCreator
import mt.think.loyalelibrary.repository.network.network_utils.performWithErrorHandling

open class HelpersRequests(private val baseUrl: String, private val applicationContext: Context) {

    /**
     * getting phone codes of countries
     */
    suspend fun getMobileCodes(): ResponseHolder<ArrayList<ApiAreaCodesDataModel>?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl).getMobileCodes()
        }
    }

    suspend fun getCountriesList(): ResponseHolder<ArrayList<ApiCountryModel>?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl).getCountriesList()
        }
    }

    suspend fun getTownsByLocale(
        schemeId: String,
        locale: String
    ): ResponseHolder<ArrayList<ApiTown>?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getTownsByLocale(schemeId, locale)
        }
    }

    suspend fun getCurrencyByFilter(): ResponseHolder<ArrayList<ApiCurrencies>?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl).getCurrencyByFilter()
        }
    }

}