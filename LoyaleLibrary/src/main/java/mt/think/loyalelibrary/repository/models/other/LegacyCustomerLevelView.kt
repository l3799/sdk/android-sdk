package mt.think.loyalelibrary.repository.models.other

data class LegacyCustomerLevelView(
    val levelId: String,
    val tierId: String,
    val schemeId: String,
    val createdDate: String,
    val updatedDate: String,
)
