package mt.think.loyalelibrary.repository.models.other

data class CoordinateView (
    val latitude: Double,
    val longitude: Double
        )