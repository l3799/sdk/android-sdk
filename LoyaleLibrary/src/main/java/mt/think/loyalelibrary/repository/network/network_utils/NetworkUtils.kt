package mt.think.loyalelibrary.repository.network.network_utils

import android.content.Context
import android.net.ConnectivityManager
import com.google.gson.Gson
import mt.think.loyalelibrary.repository.models.*
import retrofit2.Response

const val BEARER = "Bearer "

fun isConnectedToInternet(context: Context?): Boolean {
    try {
        if (context != null) {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }
        return false
    } catch (e: Exception) {
        return false
    }
}

/**
 * performing api call with error handling
 *
 */
suspend fun <T> performWithErrorHandling(
    context: Context,
    apiCall: suspend () -> Response<*>
): ResponseHolder<T?> {
    return try {
        //checking internet connection
        if (!isConnectedToInternet(context)) {
            ResponseHolder.createFailedResponse(
                INTERNET_CONNECTION_ERROR_CODE,
                arrayListOf(INTERNET_CONNECTION_ERROR),
                null
            )
        } else {
            //sending request
            val response = apiCall()

            //check have request succeed
            return if (response.isSuccessful) {
                ResponseHolder.createSucceedResponse(
                    response.body() as T?,
                    HashMap(response.headers().toMultimap())
                )
            } else {
                ResponseHolder.createFailedResponse(
                    response.code(),
                    getErrorMessageFromBodyJsonString(response.errorBody()?.string()!!),
                    HashMap(response.headers().toMultimap())
                )
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
        // handling some unexpected errors
        ResponseHolder.createFailedResponse(
            UNEXPECTED_ERROR_CODE,
            arrayListOf(UNEXPECTED_ERROR),
            null
        )
    }
}

/**
 * extracting error messages from api data model response
 * @param jsonString of model of error response from server
 * @return list of strings with error messages
 */
fun getErrorMessageFromBodyJsonString(jsonString: String): ArrayList<String> {
    return Gson().fromJson(jsonString, ErrorResponse::class.java).errors
}