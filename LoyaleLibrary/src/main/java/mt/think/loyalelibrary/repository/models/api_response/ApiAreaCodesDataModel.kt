package mt.think.loyalelibrary.repository.models.api_response

data class ApiAreaCodesDataModel(
    val code: String,
    val country: String,
    val iso: String
)