package mt.think.loyalelibrary.repository.models.api_response

data class ApiTierResultView(
    val id: String,
    val schemeId: String,
    val name: String?,
    val isDefault: Boolean,
    val expiryPeriod: Int,
    val upgradePeriod: Int,
    val guaranteedPeriod: Int,
    val updatedDate: String,
    val createdDate: String,
    val levelsUpgradable: Boolean,
    val hidden: Boolean,
    val levels: ArrayList<ApiLevelResultView>
    )
