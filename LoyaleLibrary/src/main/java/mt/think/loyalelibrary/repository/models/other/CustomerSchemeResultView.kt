package mt.think.loyalelibrary.repository.models.other

import mt.think.loyalelibrary.repository.models.api_response.ApiFranchiseResultView

data class CustomerSchemeResultView(
    val id: String,
    val name: String?,
    val currency: String?,
    val description: String?,
    val website: String?,
    val color: String?,
    val imageUrl: String?,
    val pointRedemptionPerCurrency: Double,
    val updatedDate: String,
    val createdDate: String,
    val inLoyale: Boolean,
    val hidden: Boolean,
    val rounding: Int, // enum 0-3
    val billingStatus: Int, // enum 0-4
    val franchises: ArrayList<ApiFranchiseResultView?>,
    )
