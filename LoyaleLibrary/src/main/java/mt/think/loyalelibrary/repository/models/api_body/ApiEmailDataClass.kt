package mt.think.loyalelibrary.repository.models.api_body

data class ApiEmailDataClass(val email: String)
