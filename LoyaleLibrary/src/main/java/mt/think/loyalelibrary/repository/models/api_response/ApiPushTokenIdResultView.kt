package mt.think.loyalelibrary.repository.models.api_response

data class ApiPushTokenIdResultView (
    val id: String
        )