package mt.think.loyalelibrary.repository.models.other

data class LineItemInsertView(
    val id: String?,
    val quantity: Double,
    val unitPrice: String?,
    val unitPriceNumber: Double,
    val description: String?,
    val groupId: String?
)
