package mt.think.loyalelibrary.repository.models.api_response

data class ApiCurrencies (
    val name: String?,
    val code: String?,
    val symbol: String?
        )