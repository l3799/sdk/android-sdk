package mt.think.loyalelibrary.repository.network.api_requests

import android.content.Context
import mt.think.loyalelibrary.repository.models.ResponseHolder
import mt.think.loyalelibrary.repository.models.api_response.ApiLevelResultView
import mt.think.loyalelibrary.repository.models.api_response.ApiTierResultView
import mt.think.loyalelibrary.repository.network.network_utils.BEARER
import mt.think.loyalelibrary.repository.network.network_utils.RetrofitCreator
import mt.think.loyalelibrary.repository.network.network_utils.performWithErrorHandling

open class LevelRequests(private val baseUrl: String, private val applicationContext: Context) {

    suspend fun getLevel(
        id: String,
        schemeId: String,
        token: String
    ): ResponseHolder<ApiLevelResultView?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getLevel(id = id, schemeId = schemeId, authHeader = BEARER + token)
        }
    }

    suspend fun getLevelWithFilter(
        token: String,
        schemeId: String,
        filters: String,
        sorts: String,
        page: Int,
        pageSize: Int
    ): ResponseHolder<ArrayList<ApiLevelResultView>?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getLevelWithFilter(
                    authHeader = BEARER + token,
                    schemeId = schemeId,
                    filters = filters,
                    sorts = sorts,
                    page = page,
                    pageSize = pageSize
                )
        }
    }

}