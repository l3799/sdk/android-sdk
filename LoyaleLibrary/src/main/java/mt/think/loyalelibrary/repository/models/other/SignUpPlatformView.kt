package mt.think.loyalelibrary.repository.models.other

data class SignUpPlatformView(
    val name: String?,
    val version: String?,
    val device: String?,
    val appName: String?,
    val appVersion: String?,
    val comments: String?
)
