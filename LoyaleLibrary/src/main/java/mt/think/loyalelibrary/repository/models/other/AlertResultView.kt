package mt.think.loyalelibrary.repository.models.other

data class AlertResultView (
    val id: String,
    val title: String?,
    val schemeId: String,
    val subTitle: String?,
    val text: String?,
    val imageUrl: String?,
    val thumbnailUrl: String?,
    val updatedDate: String,
    val createdDate: String,
    val deepLinkId: String?,
    val deepLinkType: String?,
    val isMarketingMaterial: Boolean
    )