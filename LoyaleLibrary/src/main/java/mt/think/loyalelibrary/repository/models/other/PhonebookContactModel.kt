package mt.think.loyalelibrary.repository.models.other

import android.net.Uri

class PhonebookContactModel {
    var id: String? = null
    var name: String? = null
    var phoneList: List<Phone>? = null
    var imageUri: Uri? = null

    class Phone {
        var phoneNo: String? = null
    }
}