package mt.think.loyalelibrary.repository.models.api_body

data class ApiRequestMessage (
    val customerId: String,
    val text: String,
    val status: Int,
    val attachments: ArrayList<String>
)