package mt.think.loyalelibrary.repository.models.other

data class AddressView (
    val addressLine1: String?,
    val addressLine2: String?,
    val town: String?,
    val postCode: String?,
    val state: String?,
    val countryId: String?
        )