package mt.think.loyalelibrary.repository.models.api_response

data class ApiPointBalanceResultView(
    val id: String,
    val customerId: String?,
    val schemeId: String,
    val value: Double,
    val pointsValue: Double,
    val monetaryValue: Double,
    val history: ArrayList<Double?>,
    val updatedDate: String,
    val createdDate: String,
)
