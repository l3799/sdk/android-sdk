package mt.think.loyalesdk

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import mt.think.loyalelibrary.repository.LoyaleManager
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        CoroutineScope(Dispatchers.IO).launch {
            val auth = LoyaleManager.AuthenticationAccount(this@MainActivity, "https://api.loyale.io/", "c8cd23c0-91fe-4ad2-89d4-838bc91ce98e")
            val resylt = auth.performLogin("test@loyale.io", "testing")

            withContext(Dispatchers.Main){
                if (resylt.isSucceed()){
                    val coupon = LoyaleManager.Franchise(this@MainActivity, "https://api.loyale.io/", "c8cd23c0-91fe-4ad2-89d4-838bc91ce98e")
                    val coporesylt = coupon.getFranchiseWithFilter("${resylt.payload?.token}", "customerId==4fdbb55c-3050-4c17-b790-64b7d4e8b772,usesLeft!=0,Coupon.Until>${
                        SimpleDateFormat("yyyy-MM-dd").format(
                            Date(System.currentTimeMillis())
                        )
                    }","", 1, 1)
                    Log.e("VNAOSLKVNASKFW", "${coporesylt.payload}")
                }
            }
        }
    }
}